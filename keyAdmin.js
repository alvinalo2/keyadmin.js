/*!
* licencia de keyMod

* Copyright (c) 2014 @alvinalo2.
*
* .licenciado bajjo licencia MIT
* http://www.opensource.org/licenses/mit-license.html
*
* Esta notificación debe ser incluida en toda copia o pieza de código de este software.
*/
function keyMod()
{
    this.keyMap = {};

    document.body.onkeydown = Delegate.create(this, this.addKeyToMap);
    document.body.onkeyup = Delegate.create(this, this.removeKeyFromMap);
}

keyMod.LEFT_KEY= 'k37';
keyMod.UP_KEY = 'k38';
keyMod.RIGHT_KEY = 'k39';
keyMod.DOWN_KEY = 'k40';
keyMod.A_KEY = 'k65';
keyMod.B_KEY = 'k66';
keyMod.C_KEY = 'k67';
keyMod.D_KEY = 'k68';
keyMod.E_KEY = 'k69';
keyMod.F_KEY = 'k70';
keyMod.G_KEY = 'k71';
keyMod.H_KEY = 'k72';
keyMod.I_KEY = 'k73';
keyMod.J_KEY = 'k74';
keyMod.K_KEY = 'k75';
keyMod.L_KEY = 'k76';
keyMod.M_KEY = 'k77';
keyMod.N_KEY = 'k78';
keyMod.O_KEY = 'k79';
keyMod.P_KEY = 'k80';
keyMod.Q_KEY = 'k81';
keyMod.R_KEY = 'k82';
keyMod.S_KEY = 'k83';
keyMod.T_KEY = 'k84';
keyMod.U_KEY = 'k85';
keyMod.V_KEY = 'k86';
keyMod.W_KEY = 'k87';
keyMod.X_KEY = 'k88';
keyMod.Y_KEY = 'k89';
keyMod.Z_KEY = 'k90';
keyMod.CERO_KEY = 'k49';
keyMod.UNO_KEY = 'k49';
keyMod.DOS_KEY = 'k50';
keyMod.TRES_KEY = 'k51';
keyMod.CUATRO_KEY = 'k52';
keyMod.CINCO_KEY = 'k53';
keyMod.SEIS_KEY = 'k54';
keyMod.SIETE_KEY = 'k55';
keyMod.OCHO_KEY = 'k56';
keyMod.NUEVE_KEY = 'k57';
keyMod.F1_KEY = 'k112';
keyMod.F2_KEY = 'k113';
keyMod.F3_KEY = 'k114';
keyMod.F4_KEY = 'k115';
keyMod.F5_KEY = 'k116';
keyMod.F6_KEY = 'k117';
keyMod.F7_KEY = 'k118';
keyMod.F8_KEY = 'k119';
keyMod.F9_KEY = 'k120';
keyMod.F10_KEY = 'k121';
keyMod.F11_KEY = 'k122';
keyMod.F12_KEY = 'k123';
keyMod.F13_KEY = 'k124';
keyMod.F14_KEY = 'k125';
keyMod.F15_KEY = 'k126';
keyMod.BACKSPACE_KEY = 'k8';
keyMod.TAB_KEY = 'k9';
keyMod.ENTER_KEY = 'k13';
keyMod.SHIFT_KEY = 'k16';
keyMod.CTRL_KEY = 'k17';
keyMod.CAPLOCK_KEY = 'k20';
keyMod.ESC_KEY = 'k27';
keyMod.SPACE_KEY = 'k32';
keyMod.PAGEUP_KEY = 'k33';
keyMod.PAGEDOWN_KEY = 'k34';
keyMod.END_KEY = 'k35';
keyMod.HOME_KEY = 'k36';
keyMod.IN_KEY = 'k45';
keyMod.DEL_KEY = 'k46';
keyMod.NUMLOCK_KEY = 'k144';
keyMod.SCRLK_KEY = 'k145';
keyMod.PB_KEY = 'k19';

keyMod.prototype.add = function(e) 
{
    var key = e.keyCode;
    this.keyMap['k'+key] = true;
};

keyMod.prototype.rem = function(e) 
{
    var key = e.keyCode;
    if (this.keyMap['k'+key]) {
       this.keyMap['k'+key] = null;
    }
};

keyMod.prototype.is = function( key )
{
    return this.keyMap[key];
};
